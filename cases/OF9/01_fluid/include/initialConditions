//Initialization and computation of turbulent quantities

// step height
H                   0.10;

// parameters
Re2H                800.0;
nu0                 1.5e-5;

// reference velocity
Uref                #calc "($Re2H*$nu0)/(2.0*$H)";


// domain thickness
z                   0.02;


// number of H for tube length
L1                  45;                  
// number of H for inlet
L2                  2;

// other dimensions
hstep               #calc "-1.0*$H";
xoutlet             #calc "$L1*$H";
xinlet              #calc "-$L2*$H";



// control volumes: cvol on H
cvol                150;
cvolinlet           #calc "$cvol*$L2";
cvoltube            #calc "$cvol*$L1";


// turbulence parameters
turbulenceIntensity  0.01;

Cmu                  0.09;
L                    0.07;

turbulentKE          #calc "1.5*pow($Uref*$turbulenceIntensity, 2.0)";
turbulentEpsilon     #calc "pow($Cmu, 0.75)*pow($turbulentKE, 1.5)/($H*$L)";
turbulentOmega       #calc "pow($Cmu, -0.25)*pow($turbulentKE, 0.5)/($H*$L)";
turbulentNut         #calc "$Cmu*pow($turbulentKE, 2.0)/$turbulentEpsilon";
turbulentNutTilda    #calc "1.5*$Uref*$turbulenceIntensity*$L";


#inputMode           error

// ************************************************************************* //

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 10:04:00 2022

@author: claudio
"""

import numpy as np
import matplotlib.pyplot as plt

data_dir = '../postProcessing/sample/5204'


U_low = np.genfromtxt(data_dir+'/x_low_U_vorticityField.xy')
U_up = np.genfromtxt(data_dir+'/x_high_U_vorticityField.xy')

H = 0.1;


z_low = np.where(np.diff(np.sign(U_low[:,1])>0))[0]
z_up  = np.where(np.diff(np.sign(U_up[:,1])>0))[0]


print("X1:")
print(U_low[z_low,0]/H)

print("X2 and X3:")
print(U_up[z_up,0]/H)


plt.figure()
plt.plot(U_low[:,0]*H, U_low[:,1])
plt.plot(U_low[z_low,0]*H, U_low[z_low,1],'o')
plt.show()

plt.figure()
plt.plot(U_up[:,0]*H, U_up[:,1])
plt.plot(U_up[z_up,0]*H, U_up[z_up,1],'o')
plt.show()

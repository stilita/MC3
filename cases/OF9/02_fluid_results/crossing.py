#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 10:04:00 2022

@author: claudio
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

cv = ['CV20', 'CV30','CV40','CV50','CV60','CV70','CV80','CV100','CV125','CV150']
sample = ['5178','6530','9492','12790','15990','19754','23709','18738','14249','18620']


cvn = np.array([20.0 ,30.0, 40.0, 50.0, 60.0, 70.0, 80.0,100.0,125.0,150.0])

X1 =np.zeros_like(cvn)
X2 =np.zeros_like(cvn)
X3 =np.zeros_like(cvn)


X1b = [11.4,12.2]
X2b = [9.5, 9.8]
X3b = [20.6, 20.96]


for kk in range(len(cv)):
    data_dir = './'+cv[kk]+'/postProcessing/sample/'+sample[kk]
    
    U_low = np.genfromtxt(data_dir+'/x_low_U_vorticityField.xy')
    U_up = np.genfromtxt(data_dir+'/x_high_U_vorticityField.xy')
    
    H = 0.1;
    
    z_low = np.where(np.diff(np.sign(U_low[:,1])>0))[0]
    z_up  = np.where(np.diff(np.sign(U_up[:,1])>0))[0]
    
    print('CV '+cv[kk])
    print("X1:")
    print(U_low[z_low,0]/H)
    
    print("X2 and X3:")
    print(U_up[z_up,0]/H)
    
    X1[kk] = (U_low[z_low,0]/H)[-1]
    print(X1)
    X2[kk] = (U_up[z_up,0]/H)[0]
    print(X2)
    X3[kk] = (U_up[z_up,0]/H)[-1]
    print(X3)

fs = 20
    
fig1 = plt.figure(figsize=(40,20))
ax1 = fig1.add_subplot(131)
plt.plot(cvn, X1, label='simulations')
plt.plot([cvn[0],cvn[-1]],[X1b[0],X1b[0]],lw=2,ls='-.',color=[0.5,0,0])
plt.plot([cvn[0],cvn[-1]],[X1b[1],X1b[1]],lw=2,ls='-.',color=[0.5,0,0])
patch= ax1.add_patch(patches.Rectangle((cvn[0], X1b[0]), cvn[-1]-cvn[0], X1b[1]-X1b[0], alpha=0.25,facecolor='red',label='benchmark'))

plt.title(r'First lower reattachment point', fontsize=fs+2)
plt.xlabel('CV', fontsize=fs)
plt.ylabel(r'$X_1$', fontsize=fs)
plt.legend(fontsize=fs)
plt.grid()
plt.xlim([cvn[0],cvn[-1]])
plt.ylim([8,13])

ax2 = fig1.add_subplot(132)
plt.plot(cvn, X2, label='simulations')
plt.plot([cvn[0],cvn[-1]],[X2b[0],X2b[0]],lw=2,ls='-.',color=[0.5,0,0])
plt.plot([cvn[0],cvn[-1]],[X2b[1],X2b[1]],lw=2,ls='-.',color=[0.5,0,0])
patch= ax2.add_patch(patches.Rectangle((cvn[0], X2b[0]), cvn[-1]-cvn[0], X2b[1]-X2b[0], alpha=0.25,facecolor='red',label='benchmark'))

plt.title(r'First upper separation point', fontsize=fs+2)
plt.xlabel('CV', fontsize=fs)
plt.ylabel(r'$X_2$', fontsize=fs)
plt.legend(fontsize=fs)
plt.grid()
plt.xlim([cvn[0],cvn[-1]])
plt.ylim([8,10])

ax3 = fig1.add_subplot(133)
plt.plot(cvn, X3, label='simulations')
plt.plot([cvn[0],cvn[-1]],[X3b[0],X3b[0]],lw=2,ls='-.',color=[0.5,0,0])
plt.plot([cvn[0],cvn[-1]],[X3b[1],X3b[1]],lw=2,ls='-.',color=[0.5,0,0])
patch= ax3.add_patch(patches.Rectangle((cvn[0], X3b[0]), cvn[-1]-cvn[0], X3b[1]-X3b[0], alpha=0.25,facecolor='red',label='benchmark'))

plt.title(r'First upper reattachment point', fontsize=fs+2)
plt.xlabel('CV', fontsize=fs)
plt.ylabel(r'$X_3$', fontsize=fs)
plt.legend(fontsize=fs)
plt.grid()
plt.xlim([cvn[0],cvn[-1]])
plt.ylim([8,22])

plt.savefig('X123.png')

# #!/usr/bin/env python3
# -*- coding: utf-8 -*-
#"""
#Created on Fri Apr  8 11:28:37 2022
#
#@author: claudio
#"""

import numpy as np
import matplotlib.pyplot as plt


bench_dir = './Gartlin'

cv_all = ['CV20', 'CV30','CV40','CV50','CV60','CV70','CV80','CV100','CV125','CV150']
sample_all = ['5178','6530','9492','12790','15990','19754','23709','18738','14249','18620']


cv = ['CV20','CV40','CV70','CV100','CV150']
sample = ['5178','9492','19754','18738','18620']

H= 0.1


U14b = np.genfromtxt(bench_dir+'/U_14.txt')
V14b = np.genfromtxt(bench_dir+'/V_14.txt')

U30b = np.genfromtxt(bench_dir+'/U_30.txt')
V30b = np.genfromtxt(bench_dir+'/V_30.txt')

p14b = np.genfromtxt(bench_dir+'/p_14.txt')
p30b = np.genfromtxt(bench_dir+'/p_30.txt')


fs = 24

fig1 = plt.figure(figsize=(40,30))
ax1 = fig1.add_subplot(221)
ax2 = fig1.add_subplot(222)
ax3 = fig1.add_subplot(223)
ax4 = fig1.add_subplot(224)

#fig1.suptitle(r'Velocity profiles', fontsize=fs+4)


fig2 = plt.figure(figsize=(40,15))
ax5 = fig2.add_subplot(121)
ax6 = fig2.add_subplot(122)

#fig2.suptitle(r'Pressure profiles', fontsize=fs+4)

ax1.plot(U14b[:,1]/max(U14b[:,1]), U14b[:,0], 'P', ms=16, label='Gartlin')
ax2.plot(V14b[:,1]/max(abs(V14b[:,1])), V14b[:,0], 'P', ms=16, label='Gartlin')
ax3.plot(U30b[:,1]/max(U30b[:,1]), U30b[:,0], 'P', ms=16, label='Gartlin')
ax4.plot(V30b[:,1]/max(V30b[:,1]), V30b[:,0], 'P', ms=16, label='Gartlin')

ax5.plot(p14b[:,1], p14b[:,0], 'P', ms=16, label='Gartlin')
ax6.plot(p30b[:,1], p30b[:,0], 'P', ms=16, label='Gartlin')



for kk in range(len(cv)):


    data_dir = './'+cv[kk]+'/postProcessing/sample/'+sample[kk]

    uv14d = np.genfromtxt(data_dir+'/x_by_h_14_U_vorticityField.xy')
    uv30d = np.genfromtxt(data_dir+'/x_by_h_30_U_vorticityField.xy')
	
    p14d = np.genfromtxt(data_dir+'/x_by_h_14_p.xy')
    p30d = np.genfromtxt(data_dir+'/x_by_h_30_p.xy')
	
    #plt.figure(figsize=(40,40))
    #plt.subplot(2,2,1)
    ax1.plot(uv14d[:,1]/max(uv14d[:,1]), uv14d[:,0]/H, lw=2, label='simulation '+cv[kk])
    ax1.set_xlabel('U at 14H', fontsize=fs)
    ax1.set_ylabel(r'$\frac{y}{H}$', fontsize=fs)
    ax1.set_ylim([-1, 1])
    ax1.legend(fontsize=fs)
    ax1.grid(True)
    ax1.tick_params(axis='both', which='major', labelsize=fs-4)
    
    #plt.subplot(2,2,2)
    ax2.plot(-uv14d[:,2]/max(abs(uv14d[:,2])), uv14d[:,0]/H, lw=2, label='simulation '+cv[kk])
    ax2.set_xlabel('V at 14H', fontsize=fs)
    ax2.set_ylabel(r'$\frac{y}{H}$', fontsize=fs)
    ax2.set_ylim([-1, 1])
    ax2.legend(fontsize=fs)
    ax2.grid(True)
    ax2.tick_params(axis='both', which='major', labelsize=fs-4)
    
    #plt.subplot(2,2,3)
    ax3.plot(uv30d[:,1]/max(uv30d[:,1]), uv30d[:,0]/H, lw=2, label='simulation '+cv[kk])
    ax3.set_xlabel('U at 30H', fontsize=fs)
    ax3.set_ylabel(r'$\frac{y}{H}$', fontsize=fs)
    ax3.set_ylim([-1, 1])
    ax3.legend(fontsize=fs)
    ax3.grid(True)
    ax3.tick_params(axis='both', which='major', labelsize=fs-4)
    
    #plt.subplot(2,2,4)
    ax4.plot(uv30d[:,2]/max(uv30d[:,2]), uv30d[:,0]/H, lw=2, label='simulation '+cv[kk])
    ax4.set_xlabel('V at 30H', fontsize=fs)
    ax4.set_ylabel(r'$\frac{y}{H}$', fontsize=fs)
    ax4.set_ylim([-1, 1])
    ax4.legend(fontsize=fs)
    ax4.grid(True)
    ax4.tick_params(axis='both', which='major', labelsize=fs-4)
    
    #plt.figure(figsize=(40,20))
    #plt.subplot(1,2,1)
    ax5.plot((p14d[:,1]-max(p14d[:,1]))*(max(p14b[:,1])-min(p14b[:,1]))/(max(p14d[:,1])-min(p14d[:,1]))+max(p14b[:,1]), p14d[:,0]/H, lw=2, label='simulation '+cv[kk])
    ax5.set_xlabel('p at 14H', fontsize=fs)
    ax5.set_ylabel(r'$\frac{y}{H}$', fontsize=fs)
    ax5.set_ylim([-1, 1])
    ax5.grid(True)
    ax5.legend(fontsize=fs)
    ax5.tick_params(axis='both', which='major', labelsize=fs-4)
    
    #plt.subplot(1,2,2)
    ax6.plot(p30d[:,1]+max(p30d[:,1])+max(p30b[:,1]), p30d[:,0]/H, lw=2, label='simulation '+cv[kk])
    ax6.set_xlim([0, 0.4])
    ax6.set_xlabel('p at 30H', fontsize=fs)
    ax6.set_ylabel(r'$\frac{y}{H}$', fontsize=fs)
    ax6.set_ylim([-1, 1])
    ax6.grid(True)
    ax6.legend(fontsize=fs)
    ax6.tick_params(axis='both', which='major', labelsize=fs-4)
    
fig1.savefig('tot_'+cv[kk]+'.png')
fig2.savefig('tot_p_'+cv[kk]+'.png')
    

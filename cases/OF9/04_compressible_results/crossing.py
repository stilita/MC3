#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 10:04:00 2022

@author: claudio
"""

import numpy as np
import matplotlib.pyplot as plt

cv = ['CV20', 'CV30','CV40','CV50','CV60','CV70','CV80'] #,'CV100','CV125','CV150']
sample = ['1424', '5927','4132','10000','3016','3932','4140'] #,'18738','14249','18620']


cvn = np.array([20.0 ,30.0, 40.0, 50.0, 60.0, 70.0, 80.0]) #,100.0,125.0,150.0])

X1 =np.zeros_like(cvn)
X2 =np.zeros_like(cvn)
X3 =np.zeros_like(cvn)


X1b = [11.4,12.2]
X2b = [9.5, 9.8]
X3b = [20.6, 20.96]


for kk in range(len(cv)):
    data_dir = './'+cv[kk]+'/postProcessing/sample/'+sample[kk]
    
    U_low = np.genfromtxt(data_dir+'/x_low_U.xy')
    U_up = np.genfromtxt(data_dir+'/x_high_U.xy')
    
    H = 0.01;
    
    z_low = np.where(np.diff(np.sign(U_low[:,1])>0))[0]
    z_up  = np.where(np.diff(np.sign(U_up[:,1])>0))[0]
    
    print('CV '+cv[kk])
    print("X1:")
    print(U_low[z_low,0]/H)
    
    print("X2 and X3:")
    print(U_up[z_up,0]/H)
    
    X1[kk] = (U_low[z_low,0]/H)[-1]
    print(X1)
    X2[kk] = (U_up[z_up,0]/H)[0]
    print(X2)
    X3[kk] = (U_up[z_up,0]/H)[-1]
    print(X3)
    
plt.figure(figsize=(40,20))
plt.subplot(1,3,1)
plt.plot(cvn, X1)
plt.xlabel('cv')
plt.ylabel(r'$X_1$')

plt.subplot(1,3,2)
plt.plot(cvn, X2)
plt.xlabel('cv')
plt.ylabel(r'$X_2$')

plt.subplot(1,3,3)
plt.plot(cvn, X3)
plt.xlabel('cv')
plt.ylabel(r'$X_3$')
plt.savefig('X123.png')

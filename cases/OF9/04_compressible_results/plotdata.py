# #!/usr/bin/env python3
# -*- coding: utf-8 -*-
#"""
#Created on Fri Apr  8 11:28:37 2022
#
#@author: claudio
#"""

import numpy as np
import matplotlib.pyplot as plt


bench_dir = './Gartlin'

cv = ['CV20', 'CV30','CV40','CV50','CV60','CV70','CV80'] #,'CV100','CV125','CV150']
sample = ['1424', '5927','4132','10000','3016','3932','4140']#,'18738','14249','18620']

H= 0.01


for kk in range(len(cv)):
    data_dir = './'+cv[kk]+'/postProcessing/sample/'+sample[kk]

    U14b = np.genfromtxt(bench_dir+'/U_14.txt')
    V14b = np.genfromtxt(bench_dir+'/V_14.txt')

    uv14d = np.genfromtxt(data_dir+'/x_by_h_14_U.xy')

    U30b = np.genfromtxt(bench_dir+'/U_30.txt')
    V30b = np.genfromtxt(bench_dir+'/V_30.txt')

    uv30d = np.genfromtxt(data_dir+'/x_by_h_30_U.xy')

    plt.figure(figsize=(40,40))
    plt.title('CV: '+cv[kk])
    plt.subplot(2,2,1)
    plt.plot(U14b[:,1]/max(U14b[:,1]), U14b[:,0]*H, 'o', label='Gartlin')
    plt.plot(uv14d[:,1]/max(uv14d[:,1]), uv14d[:,0], lw=2, label='simulation')
    plt.xlabel('U at 14H')
    plt.subplot(2,2,2)
    plt.plot(V14b[:,1]/max(abs(V14b[:,1])), V14b[:,0]*H, 'o', label='Gartlin')
    plt.plot(uv14d[:,2]/max(abs(uv14d[:,2])), uv14d[:,0], lw=2, label='simulation')
    plt.xlabel('V at 14H')
    plt.subplot(2,2,3)
    plt.plot(U30b[:,1]/max(U30b[:,1]), U30b[:,0]*H, 'o', label='Gartlin')
    plt.plot(uv30d[:,1]/max(uv30d[:,1]), uv30d[:,0], lw=2, label='simulation')
    plt.xlabel('U at 30H')
    plt.subplot(2,2,4)
    plt.plot(V30b[:,1]/max(V30b[:,1]), V30b[:,0]*H, 'o', label='Gartlin')
    plt.plot(uv30d[:,2]/max(uv30d[:,2]), uv30d[:,0], lw=2, label='simulation')
    plt.xlabel('V at 30H')
    plt.savefig(cv[kk]+'.png')

import numpy as np
import matplotlib.pyplot as plt

xh = np.array([0.0,2.0,4.0,6.0,8.0,10.0,12.0,14.0,16.0,18.0,20.0,22.0,24.0,26.0,28.0,30.0,32.0,34.0,36.0,38.0,40.0,42.0,44.0,46.0,48.0,50.0,52.0,54.0,56.0,58.0,60.0])

Tx_k1 = np.array([0.267,0.208,0.174,0.150,0.126,0.099,0.082,0.084,0.094,0.107,0.119,0.129,0.136,0.140,0.143,0.145,0.148,0.151,0.153,0.156,0.158,0.161,0.163,0.165,0.168,0.170,0.172,0.174,0.176,0.178,0.180])

Tx_k10 = np.array([0.809,0.769,0.717,0.665,0.603,0.523,0.464,0.469,0.506,0.548,0.583,0.608,0.623,0.632,0.638,0.643,0.648,0.654,0.659,0.663,0.668,0.673,0.677,0.681,0.685,0.689,0.693,0.696,0.700,0.703,0.705])

Tx_k100 = np.array([0.980,0.976,0.967,0.956,0.941,0.917,0.896,0.899,0.912,0.926,0.936,0.943,0.946,0.948,0.950,0.951,0.952,0.953,0.954,0.955,0.956,0.956,0.957,0.958,0.959,0.960,0.960,0.961,0.962,0.962,0.962])

Tx_k1000 = np.array([0.998,0.998,0.997,0.996,0.994,0.991,0.989,0.989,0.991,0.992,0.993,0.994,0.994,0.995,0.995,0.995,0.995,0.995,0.995,0.995,0.995,0.996,0.996,0.996,0.996,0.996,0.996,0.996,0.996,0.996,0.996])



xh_Nu = np.array([2.0,4.0,6.0,8.0,10.0,12.0,14.0,16.0,18.0,20.0,22.0,24.0,26.0,28.0,30.0,32.0,34.0,36.0,38.0,40.0,42.0,44.0,46.0,48.0,50.0,52.0,54.0,56.0,58.0])

Nux_k1 = np.array([0.394,0.410,0.421,0.432,0.454,0.468,0.464,0.455,0.447,0.439,0.433,0.430,0.429,0.428,0.427,0.426,0.425,0.423,0.422,0.421,0.420,0.418,0.417,0.416,0.415,0.414,0.413,0.412,0.410])

Nux_k10 = np.array([1.049,1.378,1.614,1.898,2.512,3.060,2.862,2.509,2.226,2.019,1.898,1.846,1.824,1.805,1.782,1.757,1.731,1.706,1.682,1.659,1.636,1.615,1.593,1.573,1.554,1.535,1.516,1.498,1.478])

Nux_k100 = np.array([0.963,1.422,1.869,2.552,4.353,6.553,5.789,4.421,3.506,2.954,2.678,2.576,2.540,2.508,2.466,2.417,2.367,2.317,2.268,2.221,2.177,2.134,2.093,2.054,2.016,1.981,1.947,1.914,1.882])

Nux_k1000 = np.array([0.9277,1.3845,1.8501,2.6017,4.6751,7.3374,6.4113,4.7591,3.6888,3.0671,2.7655,2.6575,2.6220,2.5910,2.5479,2.4964,2.4425,2.3888,2.3367,2.2865,2.2383,2.1922,2.1483,2.1065,2.0667,2.0288,1.9928,1.9586,1.9259])

Tmin = 300.0
Tmax = 310.0

H = 0.01


muF = 1.5e-5*1.25
# Prandtl number (mu*cp/k)
PrF = 0.71
CpF = 1005.0					
# fluid thermal conductivity
kF =muF*CpF/PrF

print(kF)


#rootdir = './CV50_t30_k1'
#final_time = 30.001
rootdir = './CV20_t20_k10'
final_time = 20

fs=20

T_x_low = np.genfromtxt(rootdir+'/postProcessing/sample/fluid/{0}/x_low_T.xy'.format(final_time))
#T_x_mid = np.genfromtxt(rootdir+'/postProcessing/sample/fluid/{0}/x_mid_T.xy'.format(final_time))

#Grad = np.genfromtxt('GradientT_k1.txt',skip_header=1, delimiter=',')
Grad = np.genfromtxt('GradientT_k10.txt',skip_header=1, delimiter=',')

plt.figure(figsize=(25,15))

plt.subplot(2,1,1)
#plt.plot(xh,Tx_k1,ls='',marker='h',ms=6,label='benchmark')
plt.plot(xh,Tx_k10,ls='',marker='h',ms=6,label='benchmark')
#plt.plot(xh,Tx_k100,ls='',marker='h',ms=6,label='k=100')
#plt.plot(xh,Tx_k1000,ls='',marker='h',ms=6,label='k=1000')
plt.plot(T_x_low[:,0]/H, (T_x_low[:,1]-Tmin)/(Tmax-Tmin),label='simulation')

plt.xlabel(r'$\frac{x}{H}$',fontsize=fs)
plt.ylabel(r'$\theta$',fontsize=fs)
plt.legend(fontsize=fs)
plt.xlim([0,60])
plt.ylim([0,1])
plt.grid()
plt.title(r'Temperature',fontsize=fs+2)


plt.subplot(2,1,2)
#plt.plot(xh_Nu,Nux_k1,ls='',marker='h',ms=6,label='benchmark')
plt.plot(xh_Nu,Nux_k10,ls='',marker='h',ms=6,label='benchmark')
#plt.plot(xh_Nu,Nux_k100,ls='',marker='h',ms=6,label='k=100')
#plt.plot(xh_Nu,Nux_k1000,ls='',marker='h',ms=6,label='k=1000')
plt.plot(Grad[1:-1,11]/H,-Grad[1:-1,1]*H/(Tmax-Tmin)*2.25,label='simulation')
plt.xlabel(r'$\frac{x}{H}$',fontsize=fs)
plt.ylabel(r'$Nu$',fontsize=fs)
plt.legend(fontsize=fs)
plt.xlim([0,60])
plt.ylim([0,4])
plt.grid()


plt.title(r'Nusselt number',fontsize=fs+2)


plt.savefig('T_Nu_X_comp_CV_20_k1=.png')



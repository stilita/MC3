/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  9
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    format      ascii;
    class       volVectorField;
    object      U;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#include "$FOAM_CASE/include/initialConditions"

dimensions      [0 1 -1 0 0 0 0];

internalField   uniform ($Uref 0 0);

boundaryField
{
    inlet
    {
    
        //
    
        type            codedFixedValue;
        value           uniform (1 0 0);
        
        name            parabolicVelocity;
        

           
        code
        #{
        
            const vectorField& Cf = patch().Cf();
            vectorField& field = *this;
            
            const scalar Re2H = 800.0;
            const scalar nu0  = 1.5e-5;
            const scalar H    = 0.01;
            
            const scalar Uref = Re2H*nu0/(2*H);
            const scalar Umax = 1.5*Uref;
            
            forAll(Cf, faceI)
            {
                const scalar y =Cf[faceI][1];
                
                field[faceI] = vector(4*Umax/(pow(H,2))*(H-y)*y, 0, 0);
            }
        #};
    }

    outlet
    {
        type            zeroGradient;
    }

    "(upperWall|lowerWall)"
    {
        type            noSlip;
    }
    
    fluid_to_solid
    {
        type            noSlip;
    }
    

    "(front|back)"
    {
        type            empty;
    }
    
    #includeEtc "caseDicts/setConstraintTypes"
}


// ************************************************************************* //

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  8 11:28:37 2022

@author: claudio
"""

import numpy as np
import matplotlib.pyplot as plt

data_dir = '../postProcessing/sample/224'

H = 0.01
Ti = 400.0
Tw = 300.0

y_ad = np.array([-0.49, -0.41, -0.32, -0.24, -0.16, -0.07, 0.01, 0.090])
th_051 = np.array([0.98, 0.84, 0.72, 0.63, 0.58, 0.60, 0.39, 0.00])
th_251 = np.array([0.96, 0.68, 0.48, 0.40, 0.41, 0.45, 0.22, 0.01])
th_501 = np.array([0.95, 0.54, 0.34, 0.33, 0.35, 0.26, 0.07, 0.00])
th_801 = np.array([0.91, 0.32, 0.21, 0.18, 0.11, 0.03, 0.00, 0.00])



th_051d = np.genfromtxt(data_dir+'/x_by_h_051_p_T.xy')
th_251d = np.genfromtxt(data_dir+'/x_by_h_251_p_T.xy')
th_501d = np.genfromtxt(data_dir+'/x_by_h_501_p_T.xy')
th_801d = np.genfromtxt(data_dir+'/x_by_h_801_p_T.xy')



plt.figure()
plt.plot(th_051, y_ad, 'o', label='argentina')
plt.plot((th_051d[:,2]-Tw)/(Ti-Tw), th_051d[:,0]/H -0.5, lw=2, label='simulation')
plt.show()

plt.figure()
plt.plot(th_251, y_ad, 'o', label='argentina')
plt.plot((th_251d[:,2]-Tw)/(Ti-Tw), th_251d[:,0]/H -0.5, lw=2, label='simulation')
plt.show()

plt.figure()
plt.plot(th_501, y_ad, 'o', label='argentina')
plt.plot((th_501d[:,2]-Tw)/(Ti-Tw), th_501d[:,0]/H -0.5, lw=2, label='simulation')
plt.show()

plt.figure()
plt.plot(th_801, y_ad, 'o', label='argentina')
plt.plot((th_801d[:,2]-Tw)/(Ti-Tw), th_801d[:,0]/H -0.5, lw=2, label='simulation')
plt.show()

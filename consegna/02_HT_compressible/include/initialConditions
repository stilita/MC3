//Initialization and computation of turbulent quantities

// step height
H                   0.001;
// solid thickness
// b                   #calc "4.0*$H";

// parameters

// flow Reynolds number
Re2H				800.0;
// fluid kinematic viscosity
nuF					1.5e-5;
// fluid density
rhoF				1.25;
// Prandtl number (mu*cp/k)
PrF					0.71;
// fluid dynamic viscosity
muF					#calc "$rhoF*$nuF";	
// specific heat 
CpF					1005.0;					
// fluid thermal conductivity
kF					#calc "($PrF)/($muF*$CpF)";

// k_ratio
// k_ratio				1.0;

// Solid thermal conductivity
// kS					#calc "$k_ratio*$kF";
// Solid density
// rhoS				$rhoF;


// reference velocity
Uref                #calc "($Re2H*$nuF)/(2.0*$H)";

// temperature

Tfluid              300.0;
Tref                310.0;


// domain thickness
z                   0.002;


// number of H for tube length
L1                  60;                  
// number of H for inlet
L2                  2;
// number of H for solid
Ls                  4;

// other dimensions
hstep               #calc "-1.0*$H";
xoutlet             #calc "$L1*$H";
xinlet              #calc "-$L2*$H";
// ysolid              #calc "$hstep-$b";


// control volumes: cvol on H
cvol                50;
cvolinlet           #calc "$cvol*$L2";
cvoltube            #calc "$cvol*$L1";
// cvolsolid           #calc "$cvol*$Ls";



// turbulence parameters
turbulenceIntensity  0.01;

Cmu                  0.09;
L                    0.07;

turbulentKE          #calc "1.5*pow($Uref*$turbulenceIntensity, 2.0)";
turbulentEpsilon     #calc "pow($Cmu, 0.75)*pow($turbulentKE, 1.5)/($H*$L)";
turbulentOmega       #calc "pow($Cmu, -0.25)*pow($turbulentKE, 0.5)/($H*$L)";
turbulentNut         #calc "$Cmu*pow($turbulentKE, 2.0)/$turbulentEpsilon";
turbulentNutTilda    #calc "1.5*$Uref*$turbulenceIntensity*$L";


#inputMode           error

// ************************************************************************* //
